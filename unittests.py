import random
from train import Point, Train, TrainLine, TrainMap, TrackNode, TrainNode, DirectionVector
import unittest

class TestTrainFunctions(unittest.TestCase):

    def setUp(self):
        self.point1 = Point(1,0)
        self.point2 = Point(1, 7)
        self.point3 = Point(8, 7)
        self.vertline = TrainLine(self.point1, self.point2, 'v')
        self.horiline = TrainLine(self.point2, self.point3, 'h')
        self.map_width = 10
        self.map_height = 10
        self.train_map = TrainMap(self.map_height, self.map_width)
        self.h_train_position = Point(1,2)
        self.h_train_direction = DirectionVector(0,1)

    def test_point(self):
        # make sure the shuffled sequence does not lose any elements
        self.assertEquals(self.point1.x, 1)
        self.assertEquals(self.point1.y, 0)
        self.point1.x = 3
        self.assertEquals(self.point1.x, 3)


    def test_trainline(self):
        self.assertEquals(self.vertline.start.x, 1)
        self.assertEquals(self.vertline.start.y, 0)
        self.assertEquals(self.vertline.orientation, 'v')
        self.assertEquals(self.vertline.length, 7)
        self.vertline.orientation = None
        self.assertEquals(self.vertline.orientation, None)
        self.vertline.setOrientation()
        self.assertEquals(self.vertline.orientation, 'v')

    def test_trainmap(self):
        self.assertEquals(len(self.train_map.grid), self.map_width)
        self.assertEquals(len(self.train_map.grid[0]), self.map_height)

    def test_add_vertical_train_line(self):
        print "============================================================="
        print "vertical train line"
        self.train_map.placeTrack(self.vertline)
        grid = self.train_map.grid
        self.train_map.printGrid()
        print "============================================================="
        #should be tracks at (1,0) (1,1) (1,2) (1,3) (1,4) (1,5) (1,6) (1,7)
        self.assertTrue(isinstance(grid[0][1], TrackNode))
        self.assertTrue(isinstance(grid[1][1], TrackNode))
        self.assertTrue(isinstance(grid[2][1], TrackNode))
        self.assertTrue(isinstance(grid[3][1], TrackNode))
        self.assertTrue(isinstance(grid[4][1], TrackNode))
        self.assertTrue(isinstance(grid[5][1], TrackNode))
        self.assertTrue(isinstance(grid[6][1], TrackNode))
        self.assertTrue(isinstance(grid[7][1], TrackNode))
        # test next positions
        self.assertEquals(grid[0][1].next_pos.y, 1)
        self.assertEquals(grid[1][1].next_pos.y, 2)
        self.assertEquals(grid[2][1].next_pos.y, 3)
        self.assertEquals(grid[3][1].next_pos.y, 4)
        self.assertEquals(grid[4][1].next_pos.y, 5)
        self.assertEquals(grid[5][1].next_pos.y, 6)
        self.assertEquals(grid[6][1].next_pos.y, 7)
        self.assertEquals(grid[7][1].current_pos.y, 7)
        #test previous positions
        self.assertEquals(grid[0][1].prev_pos.y, 1)
        self.assertEquals(grid[1][1].prev_pos.y, 0)
        self.assertEquals(grid[2][1].prev_pos.y, 1)
        self.assertEquals(grid[3][1].prev_pos.y, 2)
        self.assertEquals(grid[4][1].prev_pos.y, 3)
        self.assertEquals(grid[5][1].prev_pos.y, 4)
        self.assertEquals(grid[6][1].prev_pos.y, 5)
        self.assertEquals(grid[7][1].prev_pos.y, 6)

    def test_add_horizontal_train_line(self):
        print "============================================================="
        print "horizontal train line"
        self.train_map.placeTrack(self.horiline)
        grid = self.train_map.grid
        self.train_map.printGrid()
        print "============================================================="
        #should be tracks at (1,7) (2,7) (3,7) (4,7) (5,7) (6,7) (7,7) (8,7)
        self.assertTrue(isinstance(grid[7][1], TrackNode))
        self.assertTrue(isinstance(grid[7][2], TrackNode))
        self.assertTrue(isinstance(grid[7][3], TrackNode))
        self.assertTrue(isinstance(grid[7][4], TrackNode))
        self.assertTrue(isinstance(grid[7][5], TrackNode))
        self.assertTrue(isinstance(grid[7][6], TrackNode))
        self.assertTrue(isinstance(grid[7][7], TrackNode))
        self.assertTrue(isinstance(grid[7][8], TrackNode))
        self.assertEquals(grid[7][1].next_pos.x, 2)
        self.assertEquals(grid[7][2].next_pos.x, 3)
        self.assertEquals(grid[7][3].next_pos.x, 4)
        self.assertEquals(grid[7][4].next_pos.x, 5)
        self.assertEquals(grid[7][5].next_pos.x, 6)
        self.assertEquals(grid[7][6].next_pos.x, 7)
        self.assertEquals(grid[7][7].next_pos.x, 8)
        self.assertEquals(grid[7][8].next_pos.x, 7)
        # test the previous nodes
        self.assertEquals(grid[7][1].prev_pos.x, 2)
        self.assertEquals(grid[7][2].prev_pos.x, 1)
        self.assertEquals(grid[7][3].prev_pos.x, 2)
        self.assertEquals(grid[7][4].prev_pos.x, 3)
        self.assertEquals(grid[7][5].prev_pos.x, 4)
        self.assertEquals(grid[7][6].prev_pos.x, 5)
        self.assertEquals(grid[7][7].prev_pos.x, 6)
        self.assertEquals(grid[7][8].prev_pos.x, 7)
        self.train_map.printGrid()

    def test_add_train_to_track(self):
        #add train at position (1,2) with vector (0,1)
        self.train_map.placeTrack(self.vertline)
        self.train_map.addTrain(self.h_train_position, self.h_train_direction)
        grid = self.train_map.grid
        self.assertTrue(isinstance(grid[2][1], TrainNode))
        # check the current position, prev, and next for the train
        self.assertEquals(grid[2][1].current_pos.y, self.h_train_position.y)
        self.assertEquals(grid[2][1].prev_pos.y, self.h_train_position.y-1)
        self.assertEquals(grid[2][1].next_pos.y, self.h_train_position.y+1)
        self.assertEqual(grid[2][1].direction, self.h_train_direction)

    def test_move_train_once(self):
        self.train_map.placeTrack(self.vertline)
        self.train_map.addTrain(self.h_train_position, self.h_train_direction)
        




if __name__ == '__main__':
    unittest.main()
