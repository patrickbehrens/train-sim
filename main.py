import pygame
import os
import sys
from train import *

_image_library = {}


def get_image(path):
    global _image_library
    image = _image_library.get(path)
    if image is None:
        canolicalized_path = path.replace('/', os.sep).replace('\\', os.sep)
        image = pygame.image.load(canolicalized_path)
        _image_library[path] = image
    return image


pygame.init()

# Initialize Screen
screen_width = 400
screen_height = 400
screen = pygame.display.set_mode((screen_width, screen_height))

done = False
is_blue = True
x = 30
y = 30
circle_r = 30

# Train Specific Values
# make a train map grid of size screen_width/10 x screen_height/10
map_width = screen_width/10
map_height = screen_height/10
train_map = TrainMap(map_height, map_width)
# Track segment of length ten, from point 0,0 to 0,9
line1 = TrainLine(Point(0, 0), Point(0, 9))
line2 = TrainLine(Point(3, 1), Point(8, 1))
train_map.placeTrack(line1)
train_map.placeTrack(line2)
# train_map.printGrid()
clock = pygame.time.Clock()
surface = pygame.Surface((100, 100))

while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        done = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        done = True

        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            y -= 3
        if pressed[pygame.K_DOWN]:
            y += 3
        if pressed[pygame.K_LEFT]:
            x -= 3
        if pressed[pygame.K_RIGHT]:
            x += 3


        screen.fill((0, 0, 0))
        if is_blue:
            color = (0, 128, 255)
        else:
            color = (255, 100, 0)

        train_map.drawTracks(screen)
        # pygame.draw.rect(screen, color, pygame.Rect(x, y, 60, 60))

        # screen.blit(get_image('ball.png'), (20, 20))

        pygame.display.flip()
        clock.tick(60)
