import pygame
from random import choice, randint
from vec2d import vec2d
from creeps import Creep
#game parameters

SCREEN_WIDTH, SCREEN_HEIGHT = 400, 400
BG_COLOR = 150, 150, 80
CREEP_FILENAMES = [
    'images/bluecreep.png',
    'images/pinkcreep.png',
    'images/graycreep.png']

N_CREEPS = 20
pygame.init()
screen = pygame.display.set_mode(
    (SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
clock = pygame.time.Clock()

creeps = []

for i in range(N_CREEPS):
    creeps.append(Creep(screen,
                        choice(CREEP_FILENAMES),
                        (   randint(0, SCREEN_WIDTH),
                            randint(0, SCREEN_HEIGHT)),
                        (   choice([-1, 1]),
                            choice([-1, 1])),
                        0.1))
while True:
    time_passed = clock.tick(50)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit_game()

    screen.fill(BG_COLOR)

    for creep in creeps:
        creep.update(time_passed)
        creep.blitme()

    pygame.display.flip()
