#tracktimelooptests.py
import time
from train import *


def main():
    # Map of size 20,20
    length = 30
    sleeptime = 1
      # Map of size 20,20
    map_width = 10
    map_height = 10
    train_map = TrainMap(map_height, map_width)
    # Track segment of length ten, from point 0,0 to 0,9
    line1 = TrainLine(Point(0, 0), Point(0, 9))
    line2 = TrainLine(Point(3, 1), Point(8, 1))
    train_map.placeTrack(line1)
    train_map.placeTrack(line2)
    line1_start = train_map.grid[0][0]
    line1_next = train_map.grid[1][0]
    # print line1_start
    train1 = Train(line1_start, line1_next)
    # print line1_start
    train_map.addTrain(train1)
    for i in range(length):
        time.sleep(sleeptime)
        # train_map.printGrid()
        train_map.moveTrains()
        print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

if __name__ == "__main__":
    main()
