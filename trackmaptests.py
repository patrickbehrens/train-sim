# trackmaptests.py
from train import *


def main():
    # Map of size 20,20
    map_width = 10
    map_height = 10
    train_map = TrainMap(map_height, map_width)
    # Track segment of length ten, from point 0,0 to 0,9
    line1 = TrainLine(Point(0, 0), Point(0, 9))
    line2 = TrainLine(Point(3, 1), Point(8, 1))
    train_map.placeTrack(line1)
    train_map.placeTrack(line2)
    train_map.printGrid()

if __name__ == "__main__":
    main()
