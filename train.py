import pygame


class TrainMap(object):
    """
    2D map full of Track Nodes
    The actual position of the train cars will be tracked here.
    """
    def __init__(self, width, height):
        self.grid = [["O" for i in range(width)] for j in range(height)]
        self.trainlist = []
    """
    Hmmm. Place that track!
    Add a train line section to the trainmap
    Trainline trainline - A trainline to add to the trainmap
    """
    def placeTrack(self, line):
        print "before if"
        if line.orientation == 'h':
            # set start and end lines
            start = line.start
            end = line.end
            y_row = start.y
            self.grid[start.y][start.x] = TrackNode(start, Point(start.x+1, start.y), Point(start.x+1, start.y), orientation='h')
            self.grid[end.y][end.x] = TrackNode(end, Point(end.x-1, end.y), Point(end.x-1, end.y),  orientation='h')
            for x_pos in range(start.x+1, end.x):
                # update current position with a TrackNode, set to previous node
                if x_pos > start.x:
                    prev_pos = Point(x_pos-1, y_row)
                else:
                    prev_pos = Point(x_pos+1, y_row)
                if x_pos <= end.x:
                    next_pos = Point(x_pos+1, y_row)
                else:
                    next_pos = Point(x_pos-1, y_row)

                self.grid[start.y][x_pos] = TrackNode(Point(x_pos, y_row), next_pos, prev_pos, orientation='h')
                print '+++++++++++++++++++++'
                print "start.x: " + str(start.x) + " end.x: " + str(end.x)
                print "x_pos: " + str(x_pos)
                print "current pos: " + str(self.grid[line.start.y][x_pos].current_pos)
                print "Current position: " + str(self.grid[y_row][x_pos].current_pos) + " prev pos: " + str(self.grid[y_row][x_pos].prev_pos.x) + ", " + str(self.grid[y_row][x_pos].prev_pos.x)
        elif line.orientation == 'v':
            # set start and end lines
            start = line.start
            end = line.end
            x_col = start.x
            self.grid[start.y][start.x] = TrackNode(start, Point(start.x, start.y+1), Point(start.x, start.y+1), orientation='v')
            self.grid[end.y][end.x] = TrackNode(end, Point(end.x, end.y-1), Point(end.x, end.y-1),  orientation='v')
            for y_pos in range(start.y+1, end.y):
                # update current position with a TrackNode, set to previous node
                if y_pos > start.y:
                    prev_pos = Point(x_col, y_pos-1)
                else:
                    prev_pos = Point(x_col, y_pos+1)
                if y_pos <= end.y:
                    next_pos = Point(x_col, y_pos+1)
                else:
                    next_pos = Point(x_col, y_pos-1)

                self.grid[y_pos][line.start.x] = TrackNode(Point(x_col, y_pos), next_pos, prev_pos, orientation='v')
                print '+++++++++++++++++++++'
                print "current pos: " + str(self.grid[y_pos][line.start.x].current_pos)
                print "Current position: " + str(self.grid[y_pos][x_col].current_pos) + " prev pos: " + str(self.grid[y_pos][line.start.x].prev_pos.x) + ", " + str(self.grid[y_pos][line.start.x].prev_pos.x)
                print " next pos: " + str(self.grid[y_pos][line.start.x].next_pos.x) + ", " + str(self.grid[y_pos][line.start.x].next_pos.x)
        else:
            print "The orientation isn't set correctly."

    def xFillGrid(self):
        self.grid = [['O' for i in range(len(self.grid))] for j in range(len(self.grid[0]))]

    def drawTracks(self, screen):
        for x in range(len(self.grid)):
            for y in range(len(self.grid[0])):
                if self.grid[y][x] is not "O":
                    self.drawTrackAtPosition(screen, x, y, 30)

    def drawTrackAtPosition(self, screen, x, y, track_width):
        pygame.draw.rect(screen, (0, 128, 255), pygame.Rect(x*track_width+10, y*track_width+10, track_width, track_width))

    def printGrid(self):
        s = [[str(e) for e in row] for row in self.grid]
        lens = [max(map(len, col)) for col in zip(*s)]
        fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
        table = [fmt.format(*row) for row in s]
        print '\n'.join(table)

    def printTrackNodeCoordinates(self):
        s = [[str(e) for e in row] for row in self.grid]
        lens = [max(map(len, col)) for col in zip(*s)]
        fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
        table = [fmt.format(*row) for row in s]
        print '\n'.join(table)

    """
    For each train in the list of present trains move them.
    """
    def refreshTrainLocations(self):
        for train in self.trainlist:
            self.moveTrain(train)
            print "Train moved to position: " + str(train.cur_node.current_pos) + " with next node: " + str(train.next_node.current_pos)

    def addTrain(self, pos, direction):
        track_n = self.grid[pos.y][pos.x]
        train_n = TrainNode(track_n.current_pos, track_n.next_pos, track_n.prev_pos, direction=direction)
        self.trainlist.append(train_n)
        self.grid[pos.y][pos.x] = train_n
        print "Train added at position: " + str(train_n.current_pos)+ " with prev node: " + str(train_n.prev_pos) + " with next node: " + str(train_n.current_pos)
    """
    Move the given train to the next position on the Grid.
    Train.next
    Given prev_node - TrainNode - next_node
    Will make it prev_node - newTrack - TrainNode - next_node
    TODO: make easier to read
    """
    def moveTrain(self, train):
        # BEFORE MOVE
        vec = train.direction
        

        # The next position for the train
        # next_node = self.grid[train.next_pos.y][train.next_pos.x]
        # #previous node for the train before move
        # prev_node = self.grid[train.prev_node.y][train.prev_node.x]

        # #START MOVE -  move train node to next pos
        # self.grid[train.next_pos.y][train.next_pos.x] = train
        # self.grid[train.cur_pos.y][train.cur_pos.x] = TrackNode(
        #     train.cur_pos, train.next_pos, train.prev_pos)
        # self.grid[train.next_pos.y][train.next_pos.x].next_pos = next_node.next_pos
        # self.grid[train.next_pos.y][train.next_pos.x].prev_pos = self.grid[train.cur_pos.y][train.cur_pos.x]
        # self.grid[prev_node.cur_pos.y][prev_node.cur_pos.x] = TrackNode(
        #     prev_node.cur_pos, train.current_pos, prev_node.prev_pos)
        #     # self.grid[train.position.y][train.position.x] = "T"

class TrainLine(object):
    """
    A doubley linked list of TrackNodes.]
    Keeps track of one continuous line of track nodes.

    Point start_point - Starting point of that piece of track
    Point end_point - Ending point of that piece of track.

    """
    def __init__(self, start_point, end_point, orientation='v'):
        self.start = start_point
        self.end = end_point
        self.orientation = orientation
        self.length = self.setLength()

    """
    TODO: make this flip the start/end so the start is always at an X or Y position less
    than the end. This way when placing lines you dont have all sorts of logic to flip coordniates and what not.
    """
    def setOrientation(self):
        if self.start.y == self.end.y:
            self.orientation = self.start.orientation = 'h'
        elif self.start.x == self.end.x:
            self.orientation = self.start.orientation = 'v'
        else:
            self.orientation = self.start.orientation = None

    def setLength(self):
        if self.orientation == 'h':
            length = abs(self.start.x - self.end.x)
        elif self.orientation == 'v':
            length = abs(self.start.y - self.end.y)
        else:
            length = 0
        return length

class TrackNode(object):
    """
        Represents a track node, part of a train line.
        Coordinates pointing to previous and next track node
    """
    def __init__(self, position, next_pos=None, prev_pos=None, orientation=None):
        self.current_pos = position
        self.next_pos = next_pos
        self.prev_pos = prev_pos
        self.orientation = orientation

    def setNext(next_x, next_y):
        self.next = Point(next_x, next_y)

    def setPrevious(prev_x, prev_y):
        self.prev = Point(prev_x, prev_y)

    def positionString(self, position="current"):
        if position == 'previous':
            return str(self.prev_pos)
        elif position == "next":
            return str(self.next_pos)
        else:
            return str(self.current_pos)

    def __str__(self):
        # if self.orientation == 'v':
        #     return '||'
        # else:
        #     return '='
        return str(self.next_pos)

class TrainNode(TrackNode):
    """
    Represents a train on the tracks.
    Pass the train  Position's representing the current position and the next
    position in what ever direction the train is moving.
    """
    def __init__(self, position, next_pos, prev_pos, orientation=None, direction=None):
        self.current_pos = position
        self.next_pos = next_pos
        self.prev_pos = prev_pos
        self.orientation = orientation
        self.direction = direction

    def setNext(next_x, next_y):
        self.next = Point(next_x, next_y)

    def setPrevious(prev_x, prev_y):
        self.prev = Point(prev_x, prev_y)

    """
    Move the train one position further along the track by one position
    """
    def move(self, next_pos):
        self.current_pos = self.next_pos
        self.next_pos = next_pos

    def __str__(self):
        return str(self.prev_pos)

class Train(object):

    def __init__(self, current_position, next_position):
        self.current_pos = current_position
        self.next_pos = next_position

class Point(object):
    """
    A x,y coordinate pair. Made for convience so you can do something like
    TrackNode.prev.x instead of Tracknode.prev_x or just
    get both at once like TrackNode.prev. Maybe this can
    be changed later on so it preforms better
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"

class DirectionVector(object):
    """
    A 2d vector that signifies the direction and velocity of something.
    in format (x, y). Only 4 cardinal directions for now
    (0,0) no direction
    (0,1) right
    (0,-1) left
    (1,0) down
    (-1,0) up
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

